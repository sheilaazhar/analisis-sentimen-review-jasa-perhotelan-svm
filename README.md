# Analisis Sentimen pada Review Jasa Perhotelan Menggunakan Algoritma Linear Support Vector Classification

## Preprocessing
Langkah pertama yang dilakukan dalam prediksi review jasa perhotelan ini adalah preprocessing. Pada bagian preprocessing, diperlukan beberapa library dan module seperti string, regex, nltk, dan kami melakukan beberapa tahapan, diantaranya:
- Missing Value
- Case Folding
- Cleaning
- Tokenizing
- Normalizing
- Filtering
- Stemming

## Exploratory Data Analysis
Exploratory Data Analysis (EDA) adalah proses eksplorasi data yang bertujuan untuk memahami isi dan komponen penyusun data.

## Feature Engineering
Feature Engineering adalah proses untuk memilih, membuat, atau membuat suatu features dari data yang sudah dilakukan proses preprocessing agar pada tahap modelling dapat bekerja lebih akurat dalam pemecahan masalah. Beberapa proses yang dapat dilakukan pada feature engineering yaitu word count dengan count vectorizer, dan juga TF-IDF

## Modelling
Dalam proses modelling machine learning, kami menggunakan algoritma XGBoost. Karena data yang diolah memiliki label dengan problem klasifikasi, maka kami menggunakan XGBClassifier().
Selain XGBoost, kami juga menggunakan metode SVM (Support Vector Machine). SVM merupakan salah satu metode dalam supervised learning yang biasanya digunakan untuk klasifikasi (seperti Support Vector Classification). Karena yang kami olah adalah permasalahan klasifikasi, oleh karena itu kami menggunakan Linear Support Vector Classification (SVC).

## Validation
Dalam validasi model, kami menggunakan skoring dengan menggunakan metode cross validation untuk memilih mana model yang terbaik agar dapat mencapai nilai akurasi maksimal. Skoring yang kami lakukan ada dua, yaitu skor TF-IDF dan juga skor Vector.  Dari kedua skoring tersebut, dapat dilihat bahwa skor model SVC lebih besar dari skor XGBoost. Oleh karena itu, kami menggunakan model SVC untuk melakukan prediksi kategori review.

# Prediction
Tahap terakhir yang kami lakukan sebelum submisi yaitu melakukan prediksi kategori pada review hotel dengan model SVC. Setelah dilakukan perhitungan, kami mendapat nilai akurasi pada data train sebesar 99% atau lebih tepatnya 0.9958277020463112. Dan hasil akurasi data test menghasilkan nilai 90%.
